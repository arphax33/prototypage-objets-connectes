/* ========================================== */
/* ============ GENERATING TABLE ============ */
/* ========================================== */

CREATE TABLE user(
                     id INT PRIMARY KEY NOT NULL,
                     login VARCHAR(50) UNIQUE NOT NULL,
                     password VARCHAR(255) NOT NULL
);

CREATE TABLE object(
                       id INT PRIMARY KEY NOT NULL,
                       name VARCHAR(50) NOT NULL
);

CREATE TABLE captor(
                       id INT PRIMARY KEY NOT NULL,
                       code VARCHAR(3) NOT NULL,
                       label VARCHAR(50) NOT NULL,
                       type VARCHAR(50) NOT NULL
);

CREATE TABLE measure(
                        id INT PRIMARY KEY NOT NULL,
                        value LONG NOT NULL,
                        unit VARCHAR(10) NOT NULL,
                        created_at DATETIME NOT NULL
);

/* ========================================== */
/* ============= INSERTING DATAS ============ */
/* ========================================== */
