import { Component, OnInit } from '@angular/core';
import {Device} from '../../shared/models/device.model';
import {Router} from "@angular/router";

@Component({
  selector: 'app-objects-list',
  templateUrl: './objects-list.component.html',
  styleUrls: ['./objects-list.component.scss']
})
export class ObjectsListComponent implements OnInit {

  devices = [
    {
      device: 'Thermometer'
    },
    {
      device: 'Compas'
    }
  ];
  utils = [
    {
      label: 'temperature'
    },
    {
      label: 'humiditée',
      id: 2
    },
    {
      id: 3,
      code: '03',
      label: 'c',
      type: 1,
    },
    {
      id: 4,
      code: '04',
      label: 'd',
      type: 1,
    }
  ];
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onClick(id: number) {
    this.router.navigateByUrl('captor/' + id);
  }
}
