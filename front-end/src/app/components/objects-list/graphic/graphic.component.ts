import { Component, OnInit } from '@angular/core';
import { datas } from './datas';

@Component({
  selector: 'app-graphic',
  templateUrl: './graphic.component.html',
  styleUrls: ['./graphic.component.scss']
})
export class GraphicComponent implements OnInit {
  result = [];
  info: any;

  maximum: number;
  minimum: number;
  average: number;

  unit: string;

  legend: boolean = false;
  showLabels: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  view: any[] = [700, 300];
  animations: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string;
  yAxisLabel: string;
  timeline: boolean = true;
  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  constructor() {
    Object.assign(this, {datas})
  }

  ngOnInit(): void {
    datas.forEach(data => {
      this.info = {
        label: data.label,
        type: data.type
      };
      this.result.push({
        name: data.label,
        series: []
      });

      data.series.forEach(serie => {
        const date = new Date(serie.createdAt * 1000);
        const formattedDate = date.getHours() + ':' + date.getMinutes();
        this.result[0].series.push({
          name: formattedDate,
          value: serie.value
        });
      });
    });

    this.initMeters();
    this.initGraph();
  }

  initMeters() {
    let tempMax = 0;
    let tempMin = 9999999;
    let tempAvg = 0;

    this.result[0].series.forEach(measure => {
      if (measure.value > tempMax) {
        tempMax = measure.value;
      }

      if (measure.value < tempMin) {
        tempMin = measure.value
      }

      tempAvg += measure.value;
    });

    this.maximum = tempMax;
    this.minimum = tempMin;
    this.average = tempAvg / this.result[0].series.length;
  }

  initGraph() {
    switch (this.info.type) {
      case "THERMOMETER":
        this.xAxisLabel = "Heure";
        this.yAxisLabel = "Température";
    }
  }
}
