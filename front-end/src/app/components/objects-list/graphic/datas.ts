export var datas = [
  {
    code: 'TP1',
    label: 'Température 1',
    type: 'THERMOMETER',
    series: [
      {
        value: 15,
        unit: 'C',
        createdAt: 1588685429
      },
      {
        value: 18,
        unit: 'C',
        createdAt: 1588686029
      },
      {
        value: 20,
        unit: 'C',
        createdAt: 1588686629
      },
      {
        value: 19,
        unit: 'C',
        createdAt: 1588687229
      },
      {
        value: 25,
        unit: 'C',
        createdAt: 1588687829
      },
    ]
  }
];
