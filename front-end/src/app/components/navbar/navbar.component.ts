import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  private tempUnit: string

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onToggleUnit() {

  }

  onClickHomeBtn() {
    console.log('pouet');
    this.router.navigateByUrl('/objects-list');
  }
}
