import {Captor} from './captor.model';

export interface IMeasure {
  id?: number;
  value?: number;
  unit?: string;
  createdAt?: string;
  captor?: Captor;
}

export class Measure implements IMeasure {
  constructor(
    public id?: any,
    public value?: number,
    public unit?: string,
    public createdAt?: string,
    public captor?: Captor
  ) {
    this.id = id ? id : null;
    this.value = value ? value : null;
    this.unit = unit ? unit : null;
    this.createdAt = createdAt ? createdAt : null;
    this.captor = captor ? captor : null;
  }
}
