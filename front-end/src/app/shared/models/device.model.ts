import {User} from './user.model';

export interface IDevice {
  id?: number;
  name?: string;
  user?: User;
}

export class Device implements IDevice {
  constructor(
    public id?: any,
    public name?: string,
    public user?: User
  ) {
    this.id = id ? id : null;
    this.name = name ? name : null;
    this.user = user ? user : null;
  }
}
