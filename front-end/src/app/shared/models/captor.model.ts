import {Device} from './device.model';

export interface ICaptor {
  id?: number;
  code?: string;
  label?: string;
  type?: string;
  device?: Device;
}

export class Captor implements ICaptor {
  constructor(
    public id?: any,
    public code?: string,
    public label?: string,
    public type?: string,
    public device?: Device
  ) {
    this.id = id ? id : null;
    this.code = code ? code : null;
    this.label = label ? label : null;
    this.type = type ? type : null;
    this.device = device ? device : null;
  }
}
