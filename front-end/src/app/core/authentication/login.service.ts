import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private baseUrl = environment.SERVER_API + '/users/authenticate';

  constructor(
    private http: HttpClient
  ) {
  }

  login(credentials: { password: string; login: string; }) {
    return new Promise((resolve, reject) => {
      this.http.post(this.baseUrl, credentials).subscribe((res: { token: string, expireIn: number }) => {
        if (res.token && res.token.includes('Bearer')) {
          this.setSession(res);
          resolve(res);
          return true;
        }
        reject(res);
        return false;
      }, err => {
        reject(err);
        return false;
      });
    });
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
  }

  setSession(authentication: { token: string, expireIn: number }) {
    // const expiresAt = moment().add(authentication.expireIn, 'second');
    const token = authentication.token.split(' ')[1];

    this.setToken(token);
    // this.setExpiration(JSON.stringify(expiresAt.valueOf()));
  }

  isLoggedIn() {
    if (this.getToken()) {
      return moment().isBefore(JSON.parse(this.getExpiration()));
    } else {
      return false;
    }
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getLoggedUserId() {
    return jwt_decode(this.getToken()).id;
  }

  setExpiration(expiredAt: string) {
    localStorage.setItem('expires_at', expiredAt);
  }

  getExpiration() {
    return localStorage.getItem('expires_at');
  }
}
