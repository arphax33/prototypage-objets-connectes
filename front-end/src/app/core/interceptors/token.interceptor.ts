import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginService} from '../authentication/login.service';

export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private loginService: LoginService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.loginService.getToken();

    let cloned = req.clone({
      headers: req.headers.set('Content-Type', 'application/json')
    });

    if (token) {
      cloned = cloned.clone({
        headers: cloned.headers.set('Authorization', 'Bearer ' + token)
      });
    }

    return next.handle(cloned);
  }
}
