import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ObjectsListComponent} from './components/objects-list/objects-list.component';
import {AuthGuardService} from './core/authentication/auth-guard.service';
import {LoginComponent} from './components/login/login.component';
import {GraphicComponent} from "./components/objects-list/graphic/graphic.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'objects-list',
    canActivate: [AuthGuardService],
    component: ObjectsListComponent
  },
  {
    path: 'captor/:idCaptor',
    canActivate: [AuthGuardService],
    component: GraphicComponent
  },
  {
    path: '**',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ]
})
export class AppRoutingModule { }
