const jwt = require('jsonwebtoken');
const userService = require('./user.service');
const hashingService = require('./hashing.service');


const PRIVATE_KEY = process.env.JWT_PRIVATE_KEY;
const SESSION_TIME = process.env.JWT_SESSION_TIME;

exports.authenticate = async (credentials) => {
    return userService.findByLogin(credentials.login).then(user => {
        return hashingService.verify(user.password, credentials.password).then(result => {
            if (result) {
                return 'Bearer ' + jwt.sign({id: user.id}, PRIVATE_KEY,
                    {expiresIn: SESSION_TIME});
            } else {
                return false;
            }
        });
    }).catch(err => {
        console.log(err);
    });
};
