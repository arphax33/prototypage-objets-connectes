const argon2i = require('argon2-ffi').argon2i;
const crypto = require('crypto');

exports.generateSalt = () => {
    return crypto.randomBytes(32);
};

exports.hashPassword = (password) => {
    return argon2i.hash(password, this.generateSalt());
};

exports.verify = (hash, password) => {
    return argon2i.verify(hash, password).then(correct => {
        return correct;
    }).catch(err => {
        return false;
    });
};


