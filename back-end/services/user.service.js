const {User} = require('../models/user');
const mongoose = require('mongoose');

exports.findById = async (id) => {
    return await User.findById(mongoose.Types.ObjectId(id))
        .catch(err => {
            throw err;
        });
};

exports.findByLogin = async (login) => {
    return await User.findOne({login: login})
        .catch(err => {
            throw err;
        });
};

exports.save = async (requestBody) => {
    const {error} = validateOnSignup(requestBody);

    if (error) {
        throw error;
    }

    const userData = {
        username: requestBody.username,
        password: requestBody.password
    };

    let user = new User(userData);

    return await user.save()
        .catch(err => {
            throw err
        });
};
