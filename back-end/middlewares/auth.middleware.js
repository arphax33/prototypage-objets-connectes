const jwt = require('jsonwebtoken');

const PRIVATE_KEY = process.env.JWT_PRIVATE_KEY;

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, PRIVATE_KEY);
        const id = decodedToken.id;

        // TODO: Enforce security in tokens
        if (req.body.username && req.body.username !== username) {
            throw 'Invalid username';
        } else {
            next();
        }
    } catch (err){
        console.log(err);
        res.status(403).send();
    }
};
