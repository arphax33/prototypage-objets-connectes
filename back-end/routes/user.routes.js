var express = require('express');
var router = express.Router();

const userController = require('../controllers/user.controller');
const auth = require('../middlewares/auth.middleware');

// Authenticate
router.post('/authenticate', userController.authenticate);

// GET user by id
router.get('/:id', auth, userController.findById);

module.exports = router;
