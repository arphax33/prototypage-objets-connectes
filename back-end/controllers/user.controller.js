const userService = require('../services/user.service');
const securityService = require('../services/security.service');

exports.authenticate = (req, res) => {
    securityService.authenticate(req.body)
        .then((token) => {
            if (token) {
                res.status(201).json({
                    token: token,
                    expireIn: 3600 * 24
                });
            } else {
                res.status(401).send();
            }
        })
        .catch((error) => {
            res.status(403).json(error.message);
        });
};

exports.findById = (req, res) => {
    userService.findById(req.params.id)
        .then((user) => {
            if (!user) res.status(404).send();
            else res.status(200).send(user);
        })
        .catch((err) => {
            res.status(400).send(err.message)
        });
};
