require('dotenv').config();

const BASE_URL = process.env.APP_BASE_URL;

const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const usersRouter = require('./routes/user.routes');

const path = require('path');
const mongoose = require('mongoose');

const app = express();

app.use(logger(process.env.APPLICATION_ENV));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

mongoose.connect('mongodb://' +
    process.env.DB_HOST + ':' +
    process.env.DB_PORT + "/" +
    process.env.DB_NAME, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true
}).then(() => console.log('Database connexion successful !'))
    .catch(() => console.log('Database connexion failed !'));

app.use(BASE_URL + '/users', usersRouter);

module.exports = app;
