const mongoose = require('mongoose');
const joi = require('joi');


const userSchema = new mongoose.Schema({
    login: {
        type: String,
        required: true,
        unique: true,
        minlength: 5,
        maxlength: 50
    },
    password: {
        type: String,
        required: true,
        unique: true,
        minlength: 5,
        maxlength: 255
    },
},{
    timestamps: true
});

const userValidationSchemaOnSignup = {
    login: joi.string().min(5).max(50).required(),
    password: joi.string().min(5).max(255).required()
};
function validateUserOnSignup(user) {
    return joi.validate(user, userValidationSchemaOnSignup);
}

const userValidationSchemaOnUpdate = {
    login: joi.string().min(5).max(50).required(),
    password: joi.string().min(5).max(255).required()
};
function validateUserOnUpdate(user) {
    return joi.validate(user, userValidationSchemaOnUpdate);
}

exports.User = mongoose.model('User', userSchema);
exports.validateOnSignup = validateUserOnSignup;
exports.userValidationSchemaOnSignup = userValidationSchemaOnSignup;
exports.userValidationSchemaOnUpdate = userValidationSchemaOnUpdate;
exports.validateOnUpdate = validateUserOnUpdate;
